import React from "react";

const Input = ({ ...props }) => {
  return (
    <div>
      <input
        style={{
          borderWidth: 1,
          borderColor: "black",
          borderRadius: 8,
        }}
        {...props}
      />
    </div>
  );
};

export default Input;
