import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import Input from "../components/Input";
import Button from "../components/Button";
import Gap from "../components/Gap";
import { setUser } from "../store/actionCreators";

const FormSchema = Yup.object().shape({
  email: Yup.string()
    .email("Email format is invalid")
    .required("Email is required"),
  password: Yup.string().required("Password is required"),
});

const Login = ({ navigation }) => {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: FormSchema,
    onSubmit: (values) => submit(values),
  });

  const submit = async (values) => {
    dispatch(setUser(values));
    alert(JSON.stringify(values));
    alert("Success login");
  };

  return (
    <div>
      <Input
        placeholder="name@email.com"
        value={formik.values.email}
        onChange={(e) => formik.setFieldValue("email", e.target.value)}
      />
      <Input
        type="password"
        placeholder="Input password here"
        value={formik.values.password}
        onChange={(e) => formik.setFieldValue("password", e.target.value)}
      />
      <Gap bottom={20} />
      <ul style={{ fontSize: 12 }}>
        {Object.keys(formik.errors).map((item, index) => (
          <li key={index}>{formik.errors[item]}</li>
        ))}
      </ul>
      <Button
        isDisabled={!formik.isValid}
        title="Login"
        onClick={formik.handleSubmit}
      />
    </div>
  );
};

export default Login;
