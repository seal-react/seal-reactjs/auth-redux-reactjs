import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "../components/Button";
import Gap from "../components/Gap";
import { setUser } from "../store/actionCreators";

const Home = () => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const logout = async (values) => {
    dispatch(setUser(null));
    alert("Success logout");
  };

  return (
    <div
      style={{
        flex: 1,
        backgroundColor: "white",
        paddingHorizontal: 20,
      }}>
      <Gap bottom={20} />
      <div
        style={{
          fontSize: 22,
          color: "black",
        }}>
        Welcome, {user?.email || "Guest"}
      </div>
      <Gap bottom={20} />
      <Button title="Logout" onClick={logout} />
    </div>
  );
};

export default Home;
