import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import Login from "./pages/Login";
import Home from "./pages/Home";

const App = () => {
  const user = useSelector((state) => state.user);

  return <div>{user ? <Home /> : <Login />}</div>;
};

export default App;
